import { Component, ElementRef, EventEmitter, forwardRef, Inject, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable, Subscription } from 'rxjs/Rx';

@Component({
  selector: 'cmx-dropdown',
  template: require('./cmx-dropdown.component.html'),
  styles: [String(require('./cmx-dropdown.component.scss'))],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CmxDropdownComponent),
      multi: true
    }
  ]
})
export class CmxDropdownComponent implements ControlValueAccessor, OnInit {
  // Necesarios Siempre NUNCA CAMBIAN
  // [displayField]="'label'" [valueField]="'value'"
  @Input() public displayField: string;
  @Input() public valueField: string;

  @Input() public remote: boolean = false;
  @Input() public clearOnSelect: boolean = false;
  @Input() public forceSelection: boolean = true;
  @Input() public localFilter: boolean = false;
  @Input() public localFilterCaseSensitive: boolean = true;
  @Input() public typeAheadDelay: number = 500;

  /* inputClass: gives styles
     class to apply to the inner input field
     headerDropdown: dropdown for headers
     formDropdown: dropdown for forms
     actionBarDropdown: dropdown for actionbar
  */

  @Input() public inputClass: string;
  // value of the placeholder attribute of the inner input field
  @Input() public placeholder: string = '';
  // class to apply to the loading icon element
  @Input() public loadingIconClass: string = 'loader';
  // class to apply to the loading icon element
  @Input() public triggerIconClass: string = 'trigger myoffice-iconos-62';
  @Input() public dataRoot: string = '';
  //member of data object which marks object as disabled (sets class and prevent selection)
  @Input() public disabledField: string = null;
  @Input() public editable: boolean = true;
  @Input() public noMatchesText: string = '';
  @Input() public Icon: string;
  @Input() public Width: number;
  @Input() public widthUnit: string = 'px';
  @Input() public title: string;

  @Output() public onQuery = new EventEmitter<string>();
  @Output() public onChange: EventEmitter<any> = new EventEmitter();
  @Output() public onCreate = new EventEmitter<string>();
  @Output() public onBlur = new EventEmitter<any>();
  @Output() public onInitValue = new EventEmitter<string>();

  @ViewChild('inputField') public _input: any;

  public hideList: boolean = true;
  public data: any[];

  private _loading: boolean = false;
  private _optionsSubscription: Subscription;
  private _aheadTimer: number;
  private _currVal: string;
  private _marked: number = null;
  private _initialData: any[];
  private _hasFocus: boolean = false;
  private _tmpVal: any;
  private _enterCued: boolean = false;
  private _noBlur: boolean = false;

  // ControlValueAccessor props
  private propagateTouch = () => { };
  private propagateChange = (_: any) => { };

  constructor(@Inject(ElementRef) private scrollElement: ElementRef) { }

  ngOnInit() { }

  @Input()
  set options(value: Observable<Object[]> | Object[]) {
    if (this._optionsSubscription) {
      this._optionsSubscription.unsubscribe();
    }

    if (value instanceof Observable) {
      const options = <Observable<Object[]>>value;
      this._optionsSubscription = options.subscribe((data: any) => {
        // todo: make dataRoot work for all depths
        if (this.dataRoot) {
          data = <Object[]>data[this.dataRoot];
        }
        this.data = this._initialData = data;
        this.loading = false;
        if (0 === this._tmpVal || this._tmpVal) {
          this.writeValue(this._tmpVal);
        }
      });
    } else {
      let data = <Object[]>value;
      this.data = this._initialData = data;
      this.loading = false;

      // If the list data change, trigger a reprocessing.
      this.loadData();
    }
  }

  @Input()
  set currVal(value: string) {
    this._currVal = value;
    this._tmpVal = null;
    this.marked = null;
    this.hideList = !this._hasFocus && !this._noBlur;

    clearTimeout(this._aheadTimer);
    if (!this._currVal) {
      this.sendModelChange(null);
    }

    this._aheadTimer = setTimeout(this.loadData.bind(this), this.typeAheadDelay);
  }

  get currVal(): string {
    return this._currVal;
  }

  set marked(value: number) {
    if (null === value) {
      this._marked = value;
    } else if (this.data && 0 <= value && this.data.length >= value - 1) {
      this._marked = value;
      // use private var to prevent query trigger
      this._currVal = this.getDisplayValue(this.data[this._marked]);
    }
    //this.scrollToMarkedElement();
  }

  private scrollToMarkedElement() {
    let el = this.scrollElement.nativeElement.querySelector(`.list .item:nth-child(${this.marked + 1})`);

    let alignToTop = false;
    if (el !== null) el.scrollIntoView(alignToTop);
  }

  get marked(): number {
    return this._marked;
  }

  set loading(loading: boolean) {
    this._loading = loading;
    if (!loading && this._enterCued) {
      this._enterCued = false;
      this.handleEnter();
    }
  }

  get loading(): boolean {
    return this._loading;
  }

  public onKeyDown(event: KeyboardEvent) {
    const code = event.which || event.keyCode;
    switch (code) {
      case 13:
        event.preventDefault();
        this.handleEnter();
        break;
      case 38:
        event.preventDefault();
        this.handleUp();
        break;
      case 40:
        event.preventDefault();
        this.handleDown();
        break;
      default:
        if (!this.editable) {
          event.preventDefault();
          event.stopImmediatePropagation();
          return false;
        }
        break;
    }
  }

  public onItemClick(index: number, item: Object) {
    if (this.isDisabled(item)) {
      return;
    }
    this._noBlur = false;
    this.marked = index;

    this.onChange.emit({
      value: this.data[this.marked].value,
      label: this.data[this.marked].label,
      currentValue: this.data[this.marked]
    });
    this.sendModelChange(this.data[this.marked]);

    if (this.clearOnSelect) {
      this.clear();
    } else {
      this.hideList = true;
    }
    if (!this.remote && !this.localFilter) {
      this.data = this._initialData;
    }
  }

  public onFieldBlur(event: FocusEvent) {
    this._hasFocus = false;
    if (this._noBlur) {
      return;
    }

    this.onBlur.emit(event);
    // timeout for hide to catch click event on list :-(
    setTimeout(() => {
      this.handleEnter();
    }, 200);

    this.propagateTouch();
  }

  public onFieldFocus() {
    this._hasFocus = true;
    this.hideList = false;
    if (!this.editable) {
      this.clear();
    }
    this.loadData();
  }

  public onMouseEnterList() {
    this._noBlur = true;
  }

  public onMouseLeaveList() {
    this._noBlur = false;
  }

  public isMarked(value: Object): boolean {
    if (null === this.marked) {
      return false;
    }
    return this.data[this.marked] === value;
  }

  public isDisabled(value: Object): boolean {
    if (!this.disabledField) {
      return false;
    }

    return !!value[this.disabledField];
  }

  private handleEnter() {
    if (!this.loading) {
      // try to determine marked (look if item is in list)
      if (!this.marked) {
        for (let i = 0; i < this.data.length; i++) {
          if (this.currVal === this.getDisplayValue(this.data[i])) {
            this.marked = i;
            break;
          }
        }
      }
      if (null === this.marked) {
        if (this.forceSelection) {
          // this.onChange.emit(null);
          // this.sendModelChange(null);
          this.clear();
        } else {
          this.onCreate.emit(this.currVal);
          //this may causes error
          this.sendModelChange(this.currVal);
        }
      } else {
        let item = this.data[this.marked];
        if (this.isDisabled(item)) {
          return;
        }
        this.onChange.emit({
          value: this.data[this.marked].value,
          label: this.data[this.marked].label,
          currentValue: this.data[this.marked]
        });
        this.sendModelChange(this.data[this.marked]);
      }

      if (this.clearOnSelect) {
        this.clear();
      } else {
        this.hideList = true;
      }
      if (!this.remote && !this.localFilter) {
        this.data = this._initialData;
      }
    } else {
      this._enterCued = true;
    }
  }

  private handleUp() {
    if (null !== this.marked && this.marked > 0) {
      this.marked--;
    } else {
      this.marked = 0;
    }
  }

  private handleDown() {
    if (null !== this.marked && this.marked >= 0) {
      if (this.marked < this.data.length - 1) {
        this.marked++;
      }
    } else {
      this.marked = 0;
    }
  }

  private clear() {
    this.currVal = '';
    this.data = [];
  }

  private getDisplayValue(val: any) {
    let result: any = val;

    if (!this.displayField || !val) {
      return null;
    }

    this.displayField.split('.').forEach(index => {
      result = result[index];
    });

    return result;
  }

  private getValueValue(val: any) {
    let result: any = val;

    if (!this.valueField || !val) {
      return val;
    }

    this.valueField.split('.').forEach(index => {
      result = result[index];
    });

    return result;
  }

  private loadData() {
    if (!this.remote) {
      if (this.localFilter) {
        this.data = this._initialData.filter(item => {
          if (!this.currVal) {
            return true;
          } else {
            if (this.localFilterCaseSensitive) {
              return -1 !== this.getDisplayValue(item).indexOf(this.currVal);
            } else {
              return (
                -1 !==
                this.getDisplayValue(item)
                  .toLowerCase()
                  .indexOf(this.currVal.toLowerCase())
              );
            }
          }
        });
      }
    } else {
      this.loading = true;
      this.onQuery.emit(this._currVal);
    }
  }

  private sendModelChange(val: any) {
    this.propagateChange(this.getValueValue(val));
  }

  private searchValueObject(value: any): any {
    if (false === value instanceof Object && this.valueField && this._initialData) {
      this._initialData.forEach(item => {
        if (value === this.getValueValue(item)) {
          value = item;
        }
      });
    }
    return value;
  }

  public onTriggerClick() {
    if (this.hideList) {
      this._input.nativeElement.focus();
      return;
    }
    this._input.nativeElement.blur();
  }

  public writeValue(value: any): void {
    value = this.searchValueObject(value);

    if (value instanceof Object && this.getDisplayValue(value)) {
      this.currVal = this.getDisplayValue(value);
    } else {
      this._tmpVal = value;
    }

    this.onInitValue.emit(value);
  }

  public registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }
}
