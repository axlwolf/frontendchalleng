// Welcome to our main file!
// we are using Webpack and ES6
// feel free to use imports and exports
// as well as ES6 code

// Try to make your whole code work via exporting
// a single function to get Hot Module Replacement
// just like app.init()
// from the good ol'days
const colors = [ 'pink', 'red', 'blue' ]
const moColors = ['yellow', 'papayawhip']

const allTheColors = [ ...colors, ...moColors ]
allTheColors.map(color => console.log(`The color is ${color}`))

/**
 * Pixel2HTML - /
 */

let msg = 'Pixel2HTML - /';

let printLog = (log) => {
  'use strict';
  return console && console.log(log);
}

printLog(msg);
/* Toggle Navigation */

const toggleNavigation = document.querySelector('.site__navigation__toggle');

toggleNavigation.addEventListener('click', ()=> {
  document.body.classList.toggle('nav--open');
});
/* Slideshow*/

let nextArrow = '<button type="button" class="slick-next"> <span class="sr-only">Show Next Slide</span> <svg width="15px" height="28px" viewBox="0 0 15 28" aria-hidden="true"> <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g transform="translate(-1305.000000, -1250.000000)" fill="#86939E"> <g transform="translate(0.000000, 1081.000000)"> <path d="M1305.46007,184.157059 C1304.84664,183.518 1304.84664,182.482 1305.46007,181.842941 L1317.55023,169.239647 C1317.85655,168.920118 1318.35393,168.920118 1318.66025,169.239647 L1319.77026,170.396706 C1320.07658,170.716235 1320.07658,171.234235 1319.77026,171.553765 L1309.34433,182.421059 C1309.03801,182.740588 1309.03801,183.259412 1309.34433,183.578118 L1319.77026,194.446235 C1320.07658,194.765765 1320.07658,195.283765 1319.77026,195.603294 L1318.66025,196.760353 C1318.35393,197.079882 1317.85655,197.079882 1317.55023,196.760353 L1305.46007,184.157059 Z" transform="translate(1312.500000, 183.000000) scale(-1, 1) translate(-1312.500000, -183.000000) "></path> </g> </g> </g> </svg> </button>';

$(document).ready(() => {
  $('.contents__slideshow').slick({
      dots: true,
      infinite: true,
      nextArrow: nextArrow
  });
});
// Happy coding from Pixel2HTML
